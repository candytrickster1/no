﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStairs : MonoBehaviour {

    public Transform player;
    public float leftBounds;
    public float rightBounds;
    public float topBounds;
    public Vector3 originalPos;

    public float speed;
    private float t;

    private bool wentDown = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {

        transform.position = new Vector3(player.position.x, transform.position.y, transform.position.z);

        if (transform.position.x <= leftBounds)
        {
            transform.position = new Vector3(leftBounds, transform.position.y, transform.position.z);
        }
        else if (transform.position.x >= rightBounds)
        {
            transform.position = new Vector3(rightBounds, transform.position.y, transform.position.z);
        }

        if (player.position.y < topBounds)
        {
            t += Time.deltaTime / speed;
            transform.position = Vector3.Lerp(transform.position, new Vector3(player.position.x, player.position.y, transform.position.z), t);
            wentDown = true;
        }
        else if (player.position.y > topBounds && wentDown)
        {
            transform.position = new Vector3(player.position.x, originalPos.y, originalPos.z);
            wentDown = false;
        }

    }
}
