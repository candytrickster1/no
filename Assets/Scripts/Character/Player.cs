﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    public float world = 1;
    public float level = 1;
    public float coins = 0;
    public float score = 0;
    public float time = 400f;
    public bool isBig = false;
    public bool isFire = false;

    public bool isDead = false;
    private bool didDie = false;
    private bool didLevelUp = false;
    public Text highfiveTotal;
    public Text scoreText;
    public Text timer;
    public Text worldText;

    public bool gotFlag = false;
    public bool bossTime = false;
    public bool gameWon = false;

    public GameObject gameOverCanvas;
    public GameObject levelWinCanvas;

    public AudioClip deathSound;
    public AudioSource themeSong;
    public AudioClip flagSound;
    public AudioClip stageClear;

    Animator animator;

    public void SavePlayer() {
        SaveSystem.SavePlayer(this);
        Debug.Log("Saving...");
    }

    public void LoadPlayer() {
        Debug.Log("loading!");
        PlayerData data = SaveSystem.LoadPlayer();
        if(data != null) {
            world = data.world;
            level = data.level;
            coins = data.coins;
            score = data.score;
            isBig = data.isBig;
            isFire = data.isFire;
            if(data.isBig){
                animator.SetBool("IsBig", data.isBig);
                animator.Play("Big Idle");
                ResetColliders();
            } else if(data.isFire){
                animator.SetBool("IsFire", data.isFire);
                animator.Play("Fire Idle");
                ResetColliders();
            }
        }
    }


    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        SaveSystem.load = true;
        if (SaveSystem.load == true)
        {
            LoadPlayer();
        }
        worldText.text = "" + world + "-" + level;
        if (coins < 10)
        {
            highfiveTotal.text = "X 0" + coins;
        }
        else
        {
            highfiveTotal.text = "X " + coins;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (coins < 10)
        {
            highfiveTotal.text = "X 0" + coins;
        }
        else
        {
            highfiveTotal.text = "X " + coins;
        }
        time -= Time.deltaTime;
        timer.text = "" + time.ToString("F0");

        if (score < 100)
        {
            scoreText.text = "000000";
        }
        else if (score < 1000)
        {
            scoreText.text = "000" + score;
        }
        else if (score < 10000)
        {
            scoreText.text = "0" + score;
        }
        else if (score < 100000)
        {
            scoreText.text = "" + score;
        }

        if (gotFlag)
        {
            GotFlag();
            gotFlag = false;
        }

        if (isDead)
        {
            Die();
        }
        if (bossTime)
        {
            StartCoroutine(GoToBoss());
        }
        if (gameWon)
        {
            WinGame();
        }
    }

    public void Die()
    {
        Debug.Log("DEATH!!");
        if (!didDie)
        {
            themeSong.Stop();
            enabled = false;
            AudioSource.PlayClipAtPoint(deathSound, gameObject.transform.position);
            StartCoroutine(GoToGameOver());
            didDie = true;
        }
    }

    public IEnumerator GoToGameOver()
    {
        yield return new WaitForSeconds(deathSound.length);
        gameOverCanvas.GetComponent<GameOver>().isGameOver = true;
    }

    public void GotFlag()
    {
        //themeSong.Stop();
        AudioSource.PlayClipAtPoint(flagSound, gameObject.transform.position);
        //gain points from extra time
    }

    public IEnumerator GoToBoss()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Boss");
    }

    public void WinGame()
    {
        if(!didLevelUp) {
            didLevelUp = true;
            level += 1;
            Debug.Log("Level after: " + level);
            SavePlayer();
            levelWinCanvas.GetComponent<GameWin>().isGameWon = true;
        }
    }

    public void ResetColliders(){
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(1.1f, 1.45f);
        gameObject.GetComponent<CircleCollider2D>().offset = new Vector2(0, -0.49f);
        gameObject.GetComponent<CircleCollider2D>().radius = 0.51f;
        gameObject.GetComponent<RyanMovement>().feetPos.transform.localPosition = new Vector3(0, -1, 0);
    }
}
