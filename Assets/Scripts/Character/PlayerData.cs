﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData{

    public float world;
    public float level;
    public float coins;
    public float score;
    public bool isBig;
    public bool isFire;

    public PlayerData (Player player) {
        world = player.world;
        level = player.level;
        coins = player.coins;
        score = player.score;
        isBig = player.isBig;
        isFire = player.isFire;
    }

}
