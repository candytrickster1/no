﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RyanMovement : MonoBehaviour {

    public float speed;
    public float jumpForce;
    private float moveInput;

    private Rigidbody2D rb;
    public bool facingRight = true;

    private bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask whatIsGround;

    private bool isJumping;
    private float jumpTimeCounter;
    public float jumpTime;

    public AudioClip jumpSound;
    private bool didJumpSound = false;

    Animator animator;
    public bool isMovable;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {

        if (isMovable)
        {
            isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

            animator.SetFloat("Speed", Mathf.Abs(moveInput));
            animator.SetBool("Grounded", isGrounded);

            moveInput = Input.GetAxis("Horizontal");
            rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

            if (facingRight == false && moveInput > 0)
            {
                Flip();
            }
            else if (facingRight == true && moveInput < 0)
            {
                Flip();
            }
        }
        else {
            moveInput = 0;
            rb.velocity = new Vector2(0, 0);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(isMovable){
            if (isGrounded == true && Input.GetKeyDown(KeyCode.Space))
            {
                if(!didJumpSound) {
                    AudioSource.PlayClipAtPoint(jumpSound, gameObject.transform.position);
                    didJumpSound = true;
                }
                isJumping = true;
                jumpTimeCounter = jumpTime;
                rb.velocity = Vector2.up * jumpForce;
            }

            if (Input.GetKey(KeyCode.Space) && isJumping == true)
            {
                if (!didJumpSound) {
                    AudioSource.PlayClipAtPoint(jumpSound, gameObject.transform.position);
                    didJumpSound = true;
                }
                if (jumpTimeCounter > 0)
                {
                    rb.velocity = Vector2.up * jumpForce;
                    jumpTimeCounter -= Time.deltaTime;
                }
                else
                {
                    isJumping = false;
                    didJumpSound = false;
                }
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                isJumping = false;
                didJumpSound = false;
            }
        }

        if(gameObject.transform.position.y < -4f) {
            if(gameObject.GetComponent<Player>().level != 2) {
                gameObject.GetComponent<Player>().Die();   
            }
        }

        if(gameObject.transform.position.x > 293.2f && gameObject.GetComponent<Player>().level == 2) {
            //gameObject.GetComponent<RyanState>().bossTime = true;
            //isMovable = false;
            gameObject.GetComponent<Player>().gameWon = true;
        }

    }

    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0f, 180f, 0);
    }


    void OnCollisionEnter2D(Collision2D coll) {
        if(coll.collider.tag == "Pole") {
            GameObject flag = GameObject.FindWithTag("Flag");
            flag.GetComponent<Flag>().startFlag = true;
        }
    }





    public void AlertObservers(string message)
    {
        if (message.Equals("TransformAnimationEnded"))
        {
            gameObject.GetComponent<Player>().ResetColliders();

            isMovable = true;
        }
    }

}
