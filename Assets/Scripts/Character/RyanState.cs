﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RyanState : MonoBehaviour {

    public bool isBig = false;
    public bool isFire = false;
    public bool isDead = false;
    private bool didDie = false;
    public float coins = 0;
    public Text highfiveTotal;
    public float score = 0;
    public Text scoreText;
    private float timer = 400f;
    public Text time;

    public bool gotFlag = false;
    public bool bossTime = false;
    public bool gameWon = false;

    public GameObject gameOverCanvas;
    public GameObject gameWinCanvas;

    public AudioClip deathSound;
    public AudioSource themeSong;
    public AudioClip flagSound;
    public AudioClip stageClear;


	// Use this for initialization
	void Start () {
        if (coins < 10) {
            highfiveTotal.text = "X 0" + coins;
        }
        else {
            highfiveTotal.text = "X " + coins;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(coins < 10){
            highfiveTotal.text = "X 0" + coins;
        } else {
            highfiveTotal.text = "X " + coins;
        }
        timer -= Time.deltaTime;
        time.text = ""+timer.ToString("F0");

        if(score < 100){
            scoreText.text = "000000";
        }else if(score < 1000) {
            scoreText.text = "000" + score;
        } else if (score < 10000) {
            scoreText.text = "0" + score;
        } else if(score < 100000) {
            scoreText.text = "" + score;
        }

        if(gotFlag) {
            //StartCoroutine(GotFlag());
            GotFlag();
            gotFlag = false;
        }

        if(isDead){
            Die();
        }
        if(bossTime) {
            StartCoroutine(GoToBoss());
        }
        if(gameWon) {
            WinGame();
        }
	}

    public void Die(){
        Debug.Log("DEATH!!");
        if(!didDie) {
            themeSong.Stop();
            enabled = false;
            AudioSource.PlayClipAtPoint(deathSound, gameObject.transform.position);
            StartCoroutine(GoToGameOver());
            didDie = true;
        }
    }

    public IEnumerator GoToGameOver() {
        yield return new WaitForSeconds(deathSound.length);
        gameOverCanvas.GetComponent<GameOver>().isGameOver = true;
    }

    public void GotFlag() {
        //themeSong.Stop();
        AudioSource.PlayClipAtPoint(flagSound, gameObject.transform.position);
        //yield return new WaitForSeconds(flagSound.length);
        //AudioSource.PlayClipAtPoint(stageClear, gameObject.transform.position);
    }

    public IEnumerator GoToBoss() {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Boss");
    }

    public void WinGame() {
        gameWinCanvas.GetComponent<GameWin>().isGameWon = true;
    }
}
