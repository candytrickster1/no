﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootSomething : MonoBehaviour {


    public Vector2 velocity;
    public Vector2 offset;

    public float shootingRate = 10f;
    private float shootCooldown;

    private Animator animator;
    public Transform firePoint;
    public GameObject bulletPrefab;
    private bool isThrowing = false;

    public AudioClip throwSound;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        shootCooldown = shootingRate;
    }
	
	// Update is called once per frame
	void Update () {
        if (shootCooldown > 0)
        {
            shootCooldown -= Time.deltaTime;
        }
        if(gameObject.GetComponent<Player>().isFire) {
            if(shootCooldown < Time.deltaTime) {
                if (Input.GetButtonDown("Fire1"))
                {
                    Shoot();
                    shootCooldown = shootingRate;
                }
                else
                {
                    isThrowing = false;
                }
            }
        }
	}

    void Shoot() {
        AudioSource.PlayClipAtPoint(throwSound, gameObject.transform.position);
        isThrowing = true;
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

    }

	void FixedUpdate() {
        animator.SetBool("IsThrowing", isThrowing);
	}


}
