﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : MonoBehaviour {

    public float speed;
    public float distance;
    public bool movingRight = true;
    public Transform groundDetection;
    public Transform wallDetection;
    Animator animator;
    private bool isDead = false;
    private bool gotShot = false;
    private float jumpHeight = 0;

    public AudioClip gotStompedSound;
    public AudioClip gotShotSound;

    void Start() {
        animator = GetComponent<Animator>();
    }

	void Update() {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        if (movingRight) {
            transform.eulerAngles = new Vector3(0, 0, 0);
        } else {
            transform.eulerAngles = new Vector3(0, -180, 0);
        }
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);
        RaycastHit2D wallInfo = Physics2D.Raycast(wallDetection.position, Vector2.right, 0.2f);
        //Debug.DrawRay(wallDetection.position, Vector2.right, Color.green);
        if(!gotShot) {
            if (groundInfo.collider == false) {
                if (movingRight) {
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    movingRight = false;
                } else {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    movingRight = true;
                }
            }

            if (wallInfo.collider == true) {
                if (movingRight) {
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    movingRight = false;
                } else {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    movingRight = true;
                }
            }
        } else {
            animator.enabled = false;
            if(jumpHeight < 10) {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 0.1f);
                jumpHeight++;
            } else {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.2f);
                jumpHeight++;
                if(jumpHeight > 52){
                    Destroy(gameObject);
                }
            }
        }

	}

    IEnumerator GetStomped() {
        animator.SetBool("IsDead", true);
        isDead = true;
        enabled = false;
        AudioSource.PlayClipAtPoint(gotStompedSound, gameObject.transform.position);
        Destroy(gameObject.GetComponent<CircleCollider2D>());
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

    public void GetShot() {
        AudioSource.PlayClipAtPoint(gotShotSound, gameObject.transform.position);
        transform.eulerAngles = new Vector3(180, 0, 0);
        GameObject.FindWithTag("Player").GetComponent<Player>().score += 100;
        gotShot = true;
    }

    void OnCollisionEnter2D(Collision2D coll) {
        foreach (ContactPoint2D hitPos in coll.contacts)
        {
            if (hitPos.normal.y < -0.1f && coll.collider.tag == "Player")
            {
                if(!isDead){
                    coll.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.up * coll.gameObject.GetComponent<RyanMovement>().jumpForce;
                    coll.gameObject.GetComponent<Player>().score += 100;
                }
                StartCoroutine(GetStomped());
            } else if(Mathf.Abs(coll.contacts[0].normal.x) > 0.1f && coll.collider.tag == "Player") {
                //hurt player
            }
        }
    }

}
