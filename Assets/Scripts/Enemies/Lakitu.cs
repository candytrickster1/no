﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lakitu : MonoBehaviour {

    public Transform player;
    public float speed;
    public float rightDist;
    public float leftDist;
    float scaleX;
    public bool facingRight;

    public float shootingRate;
    private float shootCooldown;
    private Animator animator;
    public Transform firePoint;
    public GameObject semicolonPrefab;
    private bool isThrowing = false;


	// Use this for initialization
	void Start () {
        scaleX = transform.localScale.x;
        animator = gameObject.GetComponent<Animator>();
        shootCooldown = shootingRate;
        InvokeRepeating("StartShoot", 2.5f, 2.5f);
	}

    // Update is called once per frame
    void Update()
    {
        float distance = Mathf.Round(transform.position.x - player.position.x);
        Debug.Log(distance);
        if (distance >= leftDist){
            //Debug.Log(distance);
            facingRight = false;
            transform.localScale = new Vector2(-scaleX, transform.localScale.y);
            transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }
        if(distance <= rightDist) {
            facingRight = true;
            transform.localScale = new Vector2(scaleX, transform.localScale.y);
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }

	}

    void StartShoot()
    {
        StartCoroutine(Shoot());
    }

    IEnumerator Shoot()
    {
        isThrowing = true;
        //yield return new WaitForSeconds(0.5f);
        yield return new WaitForSeconds(0.5f);
        Instantiate(semicolonPrefab, firePoint.position, firePoint.rotation);
        isThrowing = false;

    }

    void FixedUpdate()
    {
        animator.SetBool("IsDucking", isThrowing);
    }
}
