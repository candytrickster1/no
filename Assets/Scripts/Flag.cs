﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : MonoBehaviour {

    public bool startFlag = false;
    public bool didPlayerFlag = false;
    private bool hitBot = false;
    public float bot;
    private GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if(startFlag) {
            if(!didPlayerFlag) {
                player.GetComponent<Player>().gotFlag = true;
                didPlayerFlag = true;
            }
            transform.eulerAngles = new Vector3(0, -180, 0);
            gameObject.transform.position = new Vector2(246.2f, gameObject.transform.position.y);
            if(!hitBot) {
                if(gameObject.transform.position.y > bot) {
                    gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.15f);
                } else {
                    Destroy(GameObject.FindWithTag("Pole").GetComponent<Collider2D>());
                    hitBot = true;
                }
            }
        }
	}
}
