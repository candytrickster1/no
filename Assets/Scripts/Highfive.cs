﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highfive : MonoBehaviour {

    public float speed;
    public float topPos;
    public float timeAliveLimit;
    private float timeAlive = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timeAlive += 1;
        if(gameObject.transform.position.y < topPos) {
            gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + speed);
        }
        if(timeAlive >= timeAliveLimit) {
            Destroy(gameObject);
        }

	}
}
