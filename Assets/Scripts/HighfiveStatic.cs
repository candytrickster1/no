﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighfiveStatic : MonoBehaviour {

    public GameObject player;
    public AudioClip coinSound;
    //public AudioSource coinSoundSource;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            AudioSource.PlayClipAtPoint(coinSound, player.transform.position);
            player.GetComponent<Player>().coins += 1;
            player.GetComponent<Player>().score += 200;
            Destroy(gameObject);
        }
    }
}
