﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    public bool isGameOver = false;
    private bool didGameOver = false;
    public GameObject player;
    public Text highfiveTotal;
    public Text scoreText;
    public Text time;
    public GameObject gameOverCanvas;
    public AudioClip gameOverSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isGameOver) {
            Time.timeScale = 0f;
            if(!didGameOver) {
                AudioSource.PlayClipAtPoint(gameOverSound, player.transform.position);
                highfiveTotal.text = player.GetComponent<Player>().highfiveTotal.text;
                scoreText.text = player.GetComponent<Player>().scoreText.text;
                time.text = player.GetComponent<Player>().timer.text;
                gameOverCanvas.SetActive(true);
                didGameOver = true;   
            }
        }
	}

    public void ReturnToMainMenu() {
        SceneManager.LoadScene("MainMenu");
    }
}
