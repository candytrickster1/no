﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameWin : MonoBehaviour {

    public bool isGameWon = false;
    private bool didGameWin = false;
    public GameObject player;
    public Text highfiveTotal;
    public Text scoreText;
    public Text time;
    public GameObject gameWinCanvas;
    public AudioClip gameWinSound;
    public AudioSource themeSong;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isGameWon && !didGameWin) {
            themeSong.Stop();
            GameObject.Find("Pause Menu").GetComponent<PauseMenu>().enabled = false;
            AudioSource.PlayClipAtPoint(gameWinSound, player.transform.position);
            highfiveTotal.text = player.GetComponent<Player>().highfiveTotal.text;
            scoreText.text = player.GetComponent<Player>().scoreText.text;
            time.text = player.GetComponent<Player>().timer.text;
            gameWinCanvas.SetActive(true);
            Time.timeScale = 0f;
            didGameWin = true;
        }
    }

    public void Continue()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        SaveSystem.load = true;
        SceneManager.LoadScene("Transistion");
    } 

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
