﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject controlMenu;
    public bool isControlMenu = false;

    public void PlayGame(string type) {
        if(type == "load") {
            SaveSystem.load = true;  
        } else {
            SaveSystem.load = false;
        }
        SceneManager.LoadScene("Transistion");
    }

    public void Controls() {
        controlMenu.SetActive(true);
    }

    public void ControlsBack() {
        controlMenu.SetActive(false);
    }

    public void QuitGame() {
        Debug.Log("QUIT!");
        Application.Quit();
    }
}
