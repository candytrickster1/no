﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public bool isPaused;
    private bool didPause = false;
    public GameObject pauseMenuCanvas;
    public AudioSource themeSong;
    public AudioClip pauseSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(isPaused) {
            if(!didPause) {
                AudioSource.PlayClipAtPoint(pauseSound, GameObject.FindWithTag("Player").transform.position);
                didPause = true;
            }
            pauseMenuCanvas.SetActive(true);
            Time.timeScale = 0f;
            themeSong.Pause();
        } else {
            pauseMenuCanvas.SetActive(false);
            themeSong.UnPause();
            didPause = false;
            Time.timeScale = 1f;
        }

        if(Input.GetKeyDown(KeyCode.Escape)) {
            isPaused = !isPaused;
        }
	}

    public void Continue() {
        isPaused = false;
    }

    public void MainMenu() {
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame() {
        Debug.Log("QUIT!");
        Application.Quit();
    }
}
