﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoFireball : MonoBehaviour {

    public Rigidbody2D rb;
    public float speed;
    public float jumpForce;

	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, 3);
        rb.velocity = transform.right * speed;
        //rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        //if(rb.velocity.x >= 0.1f){
        //    rb.velocity = transform.right * speed;
        //} else {
        //    //rb.velocity = transform. * speed;
        //}
	}

    void OnCollisionEnter2D(Collision2D col) {
        if(col.collider.tag == "Enemy") {
            //StartCoroutine(col.gameObject.GetComponent<BasicEnemy>().GetShot());
            col.gameObject.GetComponent<BasicEnemy>().GetShot();
            Explode();
        } else {
            //gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.up * 40f;
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(rb.velocity.x) * speed , jumpForce);
        }

        if (Mathf.Abs(col.contacts[0].normal.x) > 0.01f) {
            Explode();
        }

    }

    void Explode() {
        Destroy(this.gameObject);
    }

}
