﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour {

    private Animator animator;
    public GameObject player;
    bool donePickingUp = false;

    public AudioClip powerupSound;

    void Start() {
        animator = player.GetComponent<Animator>();
    }

	void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player") && !donePickingUp) {
            Pickup();
        }
	}

    void Pickup() {
        AudioSource.PlayClipAtPoint(powerupSound, player.transform.position);
        player.GetComponent<RyanMovement>().isMovable = false;
        if(!player.GetComponent<Player>().isBig) {
            animator.SetBool("IsBig", true);
            animator.Play("Transistion");
            player.GetComponent<Player>().isBig = true;
        } else if(!player.GetComponent<Player>().isFire){
            animator.SetBool("IsFire", true);
            animator.Play("Fire Transistion");
            player.GetComponent<Player>().isFire = true;
        } else {
            //be star
        }
        Destroy(gameObject);
        donePickingUp = true;
    }
}
