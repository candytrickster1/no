﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupBlock : MonoBehaviour {

    public GameObject highFivePrefab;
    private bool didHighfive = false;
    public GameObject player;
    Color greyOut = new Color32(138, 138, 138, 255);
    SpriteRenderer spriteRenderer;
    public float hitLimit = 1;
    float timesHit = 0;

    public AudioClip hitSound;
    public AudioSource hitSoundSource;

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        hitSoundSource.clip = hitSound;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D collision) {
        if (collision.transform.tag == "Player")
        {
            // Spawn an explosion at each point of contact
            foreach (ContactPoint2D hitPos in collision.contacts)
            {
                if(hitLimit > 1) {
                    if(hitPos.normal.y > 0 && !didHighfive) {
                        GameObject highfive = Instantiate(highFivePrefab, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 0.8f), Quaternion.identity);
                        player.GetComponent<Player>().coins += 1;
                        player.GetComponent<Player>().score += 200;
                        timesHit += 1;
                        didHighfive = true;
                        hitSoundSource.Play();
                        StartCoroutine(SlowItDown());
                        if(timesHit >= hitLimit) {
                            spriteRenderer.color = greyOut;
                            didHighfive = true;
                        }
                    }
                } else if(hitPos.normal.y > 0 && !didHighfive) {
                    spriteRenderer.color = greyOut;
                    GameObject highfive =  Instantiate(highFivePrefab, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 0.8f), Quaternion.identity);
                    didHighfive = true;
                    player.GetComponent<Player>().coins += 1;
                    player.GetComponent<Player>().score += 200;
                    hitSoundSource.Play();
                }
            }
        }
	}

    IEnumerator SlowItDown() {
        yield return new WaitForSeconds(0.5f);
        if(timesHit < hitLimit) {
            didHighfive = false;
        }
    }

    void ResetHit(){
        
    }

}
