﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semicolon : MonoBehaviour {

    public Rigidbody2D rb;
    public float speed;
    private float t;
    private Vector2 startPosition;
    private Vector2 throwDestination;

	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, 3);
        startPosition = transform.position;
        throwDestination = new Vector2(GameObject.FindWithTag("Player").transform.position.x,-3.62f);
	}
	
	// Update is called once per frame
	void Update () {
        t += Time.deltaTime / speed;
        transform.position = Vector3.Lerp(startPosition, throwDestination, t);
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.tag == "Player")
        {
            //StartCoroutine(col.gameObject.GetComponent<BasicEnemy>().GetShot());
            //col.gameObject.GetComponent<BasicEnemy>().GetShot();
            Explode();
        }

    }

    void Explode()
    {
        Destroy(this.gameObject);
    }
}
