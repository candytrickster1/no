﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TransistionScene : MonoBehaviour {

    public Text highfiveTotal;
    public Text scoreText;
    public Text worldText;
    public Text mainWorldText;

    private float world = 1;
    private float level = 1;

	// Use this for initialization
	void Start () {
        Time.timeScale = 1;

        if(SaveSystem.load == true) {
            Setup();
        }
        StartCoroutine(LoadGame());
	}

	IEnumerator LoadGame() {
        Debug.Log("level : "+level);
        yield return new WaitForSeconds(3f);
        if(world == 1 && level == 1) {
            Debug.Log("level 1");
            SceneManager.LoadScene("level1");    
        } else if(world == 1 && level == 2) {
            Debug.Log("level 2");
            SceneManager.LoadScene("Level2");
        }

    }

    public void Setup() {
        PlayerData data = SaveSystem.LoadPlayer();
        if(data != null) {
            world = data.world;
            level = data.level;
            if (data.coins < 10)
            {
                highfiveTotal.text = "X 0" + data.coins;
            }
            else
            {
                highfiveTotal.text = "X " + data.coins;
            }

            if (data.score < 100)
            {
                scoreText.text = "000000";
            }
            else if (data.score < 1000)
            {
                scoreText.text = "000" + data.score;
            }
            else if (data.score < 10000)
            {
                scoreText.text = "0" + data.score;
            }
            else if (data.score < 100000)
            {
                scoreText.text = "" + data.score;
            }

            worldText.text = "" + data.world + "-" + data.level;
            mainWorldText.text = "WORLD " + data.world + "-" + data.level;     
        }

    }

}
